# stage: BUILD
FROM node:12

COPY . /server
WORKDIR /server

RUN npm install
RUN npm run-script build

# stage: SERVE
FROM node:12-slim

WORKDIR /server
COPY package.json package-lock.json ./
COPY --from=0 /server/dist ./dist

RUN npm ci --only=production

ENV NODE_ENV=production
ENV PORT=3000

CMD npm run-script serve
