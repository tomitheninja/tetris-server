import express from 'express'
import logFactory from 'debug'

import { PORT } from './config'
import { middleware } from './middleware'
import { rootRouter } from './router'

import { waitForDBInitialization } from './sequelize'
import './passport'

const app = express()
const log = logFactory('app:init')

log('Mounting handlers')
app.use(middleware)
app.use(rootRouter)
log('Done')

const start = async (port = PORT) => {
  await waitForDBInitialization
  app.listen(port, () => {
    console.log(`Server listening on http://localhost:${port}`)
  })
}

export { app, start }
