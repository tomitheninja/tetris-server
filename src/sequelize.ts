import { Sequelize } from 'sequelize-typescript'

import { User } from './model/user'
import { Authentication } from './model/authentication'
import { Session } from './model/session'

const sequelize = new Sequelize('sqlite::memory:', {
  models: [User, Authentication, Session],
})

export const waitForDBInitialization = sequelize.sync().then(() => null)
