import helmetFactory from 'helmet'

export const helmet = helmetFactory()
