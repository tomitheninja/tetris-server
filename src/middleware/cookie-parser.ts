import cookieParserFactory from 'cookie-parser'

import { COOKIE_SECRET } from '../config'

export const cookieParser = cookieParserFactory(COOKIE_SECRET)
