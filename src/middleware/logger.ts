import morgan from 'morgan'
import { RequestHandler } from 'express'

import { ENABLE_LOGS } from '../config'

export const logger: RequestHandler = ENABLE_LOGS
  ? morgan('common')
  : (req, res, next) => next()
