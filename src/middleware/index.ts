import { helmet } from './helmet'
import { logger } from './logger'
import { bodyParserJSON, bodyParserUrlEncoded } from './body-parser'
import { compression } from './compression'
import { cookieParser } from './cookie-parser'

export const middleware = [
  logger,
  compression,
  helmet,
  bodyParserUrlEncoded,
  bodyParserJSON,
  cookieParser,
]
