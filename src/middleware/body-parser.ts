import bodyParserFactory from 'body-parser'

export const bodyParserUrlEncoded = bodyParserFactory.urlencoded({
  extended: false,
})

export const bodyParserJSON = bodyParserFactory.json()
