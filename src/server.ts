// eslint-disable-next-line @typescript-eslint/no-this-alias
const __GLOBAL_THIS__ = this

const setupDevLogs = async () => {
  const DEFAULT_CONFIG = {
    DEBUG: 'app:*',
    DEBUG_COLORS: '1',
  }

  process.env = {
    ...DEFAULT_CONFIG,
    ...process.env,
  }
  // Pipe debug logs to console
  ;(await import('debug')).log = console.info.bind(__GLOBAL_THIS__)
}

async function main() {
  if (process.env.NODE_ENV !== 'production') {
    await setupDevLogs()
  }
  ;(await import('./app')).start()
}

main()
