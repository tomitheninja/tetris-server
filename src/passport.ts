import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'

import { Authentication } from './model/authentication'
import { Session } from './model/session'

import logFactory from 'debug'
import { User } from './model/user'

const log = logFactory('app:passport')

passport.serializeUser<Session, Session['sessionId']>(async (session, done) => {
  try {
    log('Serialize user')
    done(null, session.sessionId)
  } catch (err) {
    done(session)
  }
})

passport.deserializeUser<Session, Session['sessionId']>(async (token, done) => {
  try {
    log('Deserialize user')
    const session = await Session.findByPk(token, { include: [User] })
    if (!session) {
      throw new Error('Token not found')
    }
    done(null, session)
  } catch (err) {
    done(err)
  }
})

log('Adding local strategy')

const localStrategy = new LocalStrategy(
  { usernameField: 'email' },
  async (userName, password, done) => {
    try {
      log('Local-Strategy auth')
      const session = await Authentication.signIn(userName, password)
      if (!session) return done(null, false)
      done(null, session.sessionId)
    } catch (err) {
      done(err)
    }
  }
)

passport.use(localStrategy)

log('Strategy added')
