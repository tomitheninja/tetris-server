import md5 from 'md5'
import { hash, compare } from 'bcrypt'
import {
  AllowNull,
  AutoIncrement,
  BeforeCreate,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  IsEmail,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript'

import { User } from './user'
import { Session } from './session'

/**
 * @description The Authentication model contains every data necessary for sign up and sign in
 * Note that it may include historical data. ORDER BY createdAt to get the latest
 */
@Table({ underscored: true, paranoid: true })
export class Authentication extends Model<Authentication> {
  @PrimaryKey
  @AutoIncrement
  @Column
  authenticationId!: number

  @ForeignKey(() => User)
  @Column
  readonly userId!: number

  @BelongsTo(() => User)
  readonly user?: User

  /**
   * @description MD5 hashed email
   */
  @AllowNull(false)
  @IsEmail
  @Column(DataType.STRING(36)) // MD5
  readonly emailHash!: string

  @BeforeCreate
  static async hashEmail(self: Authentication) {
    self.setDataValue('emailHash', md5(self.emailHash))
  }

  /**
   * @description bCrypt hashed password
   */
  @AllowNull(false)
  @Column(DataType.STRING(60))
  readonly passwordHash!: string

  @BeforeCreate
  static async hashPassword(self: Authentication) {
    self.setDataValue('passwordHash', await hash(self.passwordHash, 12))
  }

  @CreatedAt
  @Column
  readonly createdAt!: Date

  @UpdatedAt
  @Column
  readonly updatedAt!: Date

  static async findByEmail(email: string) {
    if (email === '') return null
    return Authentication.findOne({
      include: [User],
      where: { emailHash: md5(email) },
    })
  }

  static async signIn(email: string, password: string) {
    if (email === '' || password === '') return null
    const auth = await this.findByEmail(email)
    if (!auth) return null // No user with this email
    if (await compare(password, auth.passwordHash)) {
      // Success
      if (!auth.user) {
        throw new Error()
      }
      return Session.createNew(auth)
    }
    return null // Bad password
  }

  static async signUp(email: string, password: string, userName?: string) {
    if (email === '' || password === '') return null
    if (!(await this.findByEmail(email))) {
      // Create user
      const user = await new User({ userName }).save()
      await new Authentication({
        userId: user.userId,
        emailHash: email,
        passwordHash: password,
      }).save()
    }

    // Sign in
    return this.signIn(email, password)
  }
}
