import {
  AllowNull,
  AutoIncrement,
  Column,
  CreatedAt,
  DataType,
  Default,
  HasMany,
  Is,
  Length,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript'

import { Authentication } from './authentication'
import { Session } from './session'

/**
 * @description The User model contains data necessary for authentication and holds long living data
 */
@Table({ underscored: true, paranoid: true })
export class User extends Model<User> {
  @PrimaryKey
  @AutoIncrement
  @AllowNull(false)
  @Column
  readonly userId!: number

  @AllowNull(false)
  @Default(DataType.UUIDV4)
  @Length({ min: 5, max: 50, msg: 'username should contain 5-50 characters' })
  @Is({
    args: [/^[a-z0-9][a-z0-9\-_ ]+/i],
    msg: 'userName should not contain special characters',
  })
  @Column(DataType.STRING(50))
  userName!: string

  @CreatedAt
  @Column
  readonly createdAt!: Date

  @UpdatedAt
  @Column
  readonly updatedAt!: Date

  @HasMany(() => Authentication)
  authentications!: Authentication[]

  @HasMany(() => Session)
  sessions!: Session[]
}
