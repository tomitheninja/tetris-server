import {
  Model,
  DataType,
  Table,
  Column,
  ForeignKey,
  AllowNull,
  PrimaryKey,
  Default,
  CreatedAt,
  UpdatedAt,
  BelongsTo,
} from 'sequelize-typescript'

import { User } from './user'
import { Authentication } from './authentication'

@Table({ underscored: true, paranoid: true })
export class Session extends Model<Session> {
  @ForeignKey(() => User)
  @AllowNull(false)
  @Column
  readonly userId!: number

  @BelongsTo(() => User)
  readonly user!: User

  @PrimaryKey
  @Default(DataType.UUIDV4)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  readonly sessionId!: string

  @CreatedAt
  @Column
  readonly createdAt!: Date

  @UpdatedAt
  @Column
  readonly updatedAt!: Date

  static async createNew(auth: Authentication) {
    const { sessionId } = await new Session({ userId: auth.userId }).save()
    return Session.findByPk(sessionId, {
      include: [User],
    })
  }
}
