import logFactory from 'debug'

const log = logFactory('app:config')

class InvalidConfigError extends Error {}

class SecretNotStrongError extends Error {}

function assertSecureSecret(secret: string, keyName: string) {
  if (!/[a-z]/.test(secret)) {
    throw new SecretNotStrongError(
      keyName + ' should contain a lowercase character'
    )
  }
  if (!/[A-Z]/.test(secret)) {
    throw new SecretNotStrongError(
      keyName + ' should contain an uppercase character'
    )
  }
  if (!/[0-9]/.test(secret)) {
    throw new SecretNotStrongError(
      keyName + ' should contain a numeric character'
    )
  }
  if (/(pass)|(1234)|(keyboard)/.test(secret)) {
    throw new SecretNotStrongError(
      keyName + ' should not contain a common pattern'
    )
  }
  if (secret.length < 10) {
    throw new SecretNotStrongError(
      keyName + ' should contain at least 10 characters'
    )
  }
}

log('Loading config')

export const ENV = (() => {
  const x = process.env.NODE_ENV?.toLowerCase() ?? 'development'
  switch (x) {
    case 'production':
    case 'development':
    case 'test':
      return x
    default:
      throw new InvalidConfigError('Unknown $NODE_ENV')
  }
})()

export const PORT = (() => {
  if (ENV === 'test') {
    return null
  }

  if (!process.env.PORT) {
    throw new InvalidConfigError('Missing $PORT')
  }
  return process.env.PORT
})()

export const ENABLE_LOGS = process.env.ENABLE_LOGS !== '0'

export const COOKIE_SECRET = (() => {
  const { COOKIE_SECRET } = process.env
  if (!COOKIE_SECRET || assertSecureSecret(COOKIE_SECRET, 'COOKIE_SECRET')) {
    throw new InvalidConfigError('Missing $COOKIE_SECRET')
  }
  return COOKIE_SECRET
})()

log('Config loaded')
